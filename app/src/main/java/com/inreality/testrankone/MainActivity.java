package com.inreality.testrankone;

import io.rankone.rocsdk.*;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.graphics.drawable.AnimationDrawable;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.common.thermalimage.HotImageCallback;
import com.common.thermalimage.TemperatureBitmapData;
import com.common.thermalimage.TemperatureData;
import com.common.thermalimage.ThermalImageUtil;
import com.google.gson.Gson;
import com.splunk.mint.Mint;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements Camera.PreviewCallback {
    private static final int Image_Capture_Code = 1;

//    ImageView iv, iv2;
//    TextView tv;
//    Button btn;
//    Button saveBtn;

    //View
    ImageView face_mark_iv;
    AnimationDrawable faceMarkAnimation;
    TextView title_tv;

    LinearLayout temp_layout;
    TextView temp_tv;
    ImageView temp_iv;


    ThermalImageUtil temperatureUtil;
    List<Template> templateList = new ArrayList<Template>();

    //    private int noFaceCount = 0;
    private long lastFaceTimestamp;

    private Camera cameraObject;
    private ShowCamera showCamera;
    private Boolean detectionRunning = false;

    static {
        System.loadLibrary("_roc");
    }

    public Camera isCameraAvailiable() {
        Camera object = null;
        try {
            object = Camera.open(0);
        } catch (Exception e) {
        }
        return object;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);


        Mint.initAndStartSession(this.getApplication(), "5cd2e163");

//        getAndroidId();
        initTemperatureUtils();
        initFaceReg();

        initView();
        showCamera();

        loadFace();
//        enrollAndCheck();
//        detectFace();

    }

    private byte[] template2Byte(roc_template template) {
        SWIGTYPE_p_size_t buffer_size = roc.new_size_t();
        roc.roc_ensure(roc.roc_flattened_bytes(template, buffer_size));
        byte[] native_buffer = new byte[(int) roc.size_t_value(buffer_size)];
        roc.roc_ensure(roc.roc_flatten(template, native_buffer));
        roc.delete_size_t(buffer_size);
        return native_buffer;
    }

    private roc_template byte2Template(byte[] native_buffer) {
        roc_template template = new roc_template();
        roc.roc_ensure(roc.roc_unflatten(native_buffer, template));
        return template;
    }

    private void byte2File(byte[] bytesArray, File file) throws IOException {
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(bytesArray);
        fos.close();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private byte[] File2Byte(File file) throws IOException {
        byte[] fileContent = Files.readAllBytes(file.toPath());
        return fileContent;
    }

    private void loadFace() {
        Bitmap bitmap = getBitmapFromAsset(this, "jacky.jpg");
        roc_template template = getTemplateFromBitmap(bitmap);
        if (template != null) {
            Template t = new Template("Jacky", template);
            templateList.add(t);
        }

        bitmap = getBitmapFromAsset(this, "shylesh.jpg");
        roc_template template1 = getTemplateFromBitmap(bitmap);
        if (template != null) {
            Template t = new Template("Shylesh", template1);
            templateList.add(t);
        }

//        roc.roc_free_template(template);
    }

    private void initView() {
        face_mark_iv = findViewById(R.id.face_mark_iv);

        temp_tv = findViewById(R.id.temp_tv);
        temp_layout = findViewById(R.id.temp_layout);
        temp_iv = findViewById(R.id.temp_iv);

        title_tv = findViewById(R.id.title_tv);

        face_mark_iv.setBackgroundResource(R.drawable.face_mark_animation);
        faceMarkAnimation = (AnimationDrawable) face_mark_iv.getBackground();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            hideSystemUI();
        }
    }

    private void hideSystemUI() {
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    private void showCamera() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 100);
        } else {
//            Intent cInt = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//            startActivityForResult(cInt, Image_Capture_Code);

            cameraObject = isCameraAvailiable();
            showCamera = new ShowCamera(this, cameraObject, this);
            FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
            preview.addView(showCamera);
        }
    }


    private void initTemperatureUtils() {
        temperatureUtil = new ThermalImageUtil(this);
    }

    private void showTip(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, msg, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void showTemp(final float temp, final Boolean isWarning) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                temp_layout.setVisibility(View.VISIBLE);
                temp_tv.setText(temp + "°C | " + String.format("%.1f", convertC2F((temp))) + "°F");
                if (isWarning) {
                    temp_layout.setBackgroundColor(Color.parseColor("#FF0000"));
                    temp_iv.setImageResource(R.drawable.fail);
                } else {
                    temp_layout.setBackgroundColor(Color.parseColor("#9cb92d"));
                    temp_iv.setImageResource(R.drawable.pass);
                }
            }
        });
    }

    private void updateTitle(final String msg) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                title_tv.setText(msg);
            }
        });
    }

    private float convertC2F(float c) {
        return c * 9 / 5 + 32;
    }

//    private void initView() {
//        tv = findViewById(R.id.textView);
//        iv = findViewById(R.id.imageView);
//        iv2 = findViewById(R.id.imageView2);
//        btn = findViewById(R.id.button);
//        saveBtn = findViewById(R.id.save_btn);
//        btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                getTemp();
//            }
//        });
//
//        saveBtn.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
////                if ((template.getAlgorithm_id() & (long) roc_algorithm_options.ROC_INVALID) != 0) {
////                    //no face
////                } else {
////                    // have face
////                    Log.v("TAG", "===save template");
////                    templateList.add(template);
////                }
//            }
//        });
//    }

    private Boolean tempRunning = false;

    private void getTemp() {
        if (tempRunning)
            return;
        new Thread(new Runnable() {
            @Override
            public void run() {
                tempRunning = true;
//                Rect rect = new Rect(0, 0, 0, 0);
                TemperatureData temperatureData = temperatureUtil.getDataAndBitmap(50, true, new HotImageCallback.Stub() {
                    @Override
                    public void onTemperatureFail(String e) {
                        Log.i("getDataAndBitmap", "onTemperatureFail " + e);
                        showTip("Failed to get temperature:  " + e);
                        tempRunning = false;
                    }

                    @Override
                    public void getTemperatureBimapData(final TemperatureBitmapData data) {
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
////                                iv2.setImageBitmap(data.getBitmap());
//                            }
//                        });
                    }

                });
                if (temperatureData != null) {
                    if (temperatureData.isUnusualTem()) {
                        showTemp(temperatureData.getTemperature(), true);
                    } else {
                        showTemp(temperatureData.getTemperature(), false);
                    }
                }
                tempRunning = false;
            }
        }).start();
    }

    private void enrollAndCheck() {
        Log.v("aa", "start");
        Bitmap bitmap1 = getBitmapFromAsset(this, "josh1_s.jpg");
        Log.v("aa", "read bitmap1");
        Bitmap bitmap2 = getBitmapFromAsset(this, "josh2_s.jpg");
        Log.v("aa", "read bitmap2");
        roc_image ri1 = fromBitmap(bitmap1);
        roc_image ri2 = fromBitmap(bitmap2);
        Log.v("aa", "convert 2 image to roc image");

        roc_template templates1 = new roc_template();
        roc_template templates2 = new roc_template();
        long start = System.currentTimeMillis();
        roc.roc_represent(ri1, roc_algorithm_options.ROC_FR, 20, 3, 0.02f,
                roc.ROC_SUGGESTED_MIN_QUALITY,
                templates1);

        if ((templates1.getAlgorithm_id() & (long) roc_algorithm_options.ROC_INVALID) != 0) {
//            Log.v("TAG", "!!!!!no face");
        }

        long end = System.currentTimeMillis();
        long diff = end - start;
        Log.v("aa", "convert to template time : " + diff);

        roc.roc_represent(ri2, roc_algorithm_options.ROC_FR, 20, 3, 0.02f,
                roc.ROC_SUGGESTED_MIN_QUALITY,
                templates2);


        Log.v("aa", "convert 2 roc image to roc template");

        SWIGTYPE_p_float f = roc.new_roc_similarity();

        roc.roc_compare_templates(templates1, templates2, f);

        Log.v("aa", "compare 2 templates");

        Log.v("aa", "=====" + roc.roc_similarity_value(f));
    }

    private void getAndroidId() {
        String android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.v("abc", "======" + android_id);
    }

    private void detectFace() {
        Bitmap bitmap = getBitmapFromAsset(this, "josh1_s.jpg");
        roc_image ri = fromBitmap(bitmap);
        roc_template templates = new roc_template();
        roc.roc_represent(ri, roc_algorithm_options.ROC_FRONTAL | roc_algorithm_options.ROC_FR, 20, 3, 0.02f,
                roc.ROC_SUGGESTED_MIN_QUALITY,
                templates);

        if ((templates.getAlgorithm_id() & (long) roc_algorithm_options.ROC_INVALID) != 0) {
//            Log.v("TAG", "!!!!!no face");
        }

        int left = (int) templates.getDetection().getX() - ((int) templates.getDetection().getWidth() / 2);
        int top = (int) templates.getDetection().getY() - ((int) templates.getDetection().getHeight() / 2);
        int right = (int) templates.getDetection().getX() + ((int) templates.getDetection().getWidth() / 2);
        int bottom = (int) templates.getDetection().getY() + ((int) templates.getDetection().getHeight() / 2);

        Canvas canvas = new Canvas(bitmap);
        Paint p = new Paint();
        p.setStrokeWidth(10f);
        p.setColor(Color.RED);
        p.setStyle(Paint.Style.STROKE);
        canvas.drawRect(left, top, right, bottom, p);

        canvas.drawPoint(templates.getDetection().getX(), templates.getDetection().getY(), p);


//        iv.setImageBitmap(bitmap);
    }

    public Bitmap toBitmap(roc_image image) {
        byte[] bytes = new byte[4 * (int) image.getWidth() * (int) image.getHeight()];
        roc.roc_to_rgba(image, bytes);
        Bitmap bitmap = Bitmap.createBitmap((int) image.getWidth(), (int) image.getHeight(), Bitmap.Config.ARGB_8888);
        bitmap.copyPixelsFromBuffer(ByteBuffer.wrap(bytes));
        return bitmap;
    }


    public Bitmap getBitmapFromAsset(Context context, String filePath) {
        AssetManager assetManager = context.getAssets();
        InputStream istr;
        Bitmap bitmap = null;
        try {
            istr = assetManager.open(filePath);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inMutable = true;
            bitmap = BitmapFactory.decodeStream(istr, null, options);
        } catch (IOException e) {
            // handle exception
        }
        return bitmap;
    }

    public roc_image fromBitmap(Bitmap bitmap) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(bitmap.getRowBytes() * bitmap.getHeight());
        bitmap.copyPixelsToBuffer(byteBuffer);
        roc_image image = new roc_image();
        roc.roc_from_rgba(byteBuffer.array(), bitmap.getWidth(), bitmap.getHeight(), bitmap.getRowBytes(), image);
        return image;
    }

    public void initFaceReg() {
        ensure(roc.roc_preinitialize_android(this));
        ensure(roc.roc_initialize(readAssetFile("ROC.lic"), null));
    }

    public String readAssetFile(String fileName) {
        try {
            InputStream is = getAssets().open(fileName);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            return new String(buffer);
        } catch (IOException ioe) {
            return new String();
        }
    }


    public static void ensure(String error) {
        if (error != null) {
            Log.e("ROC SDK", error);
            System.exit(1);
        }
    }


    public static Bitmap RotateBitmap(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }


    private Bitmap getBitmapFromFrame(byte[] data, Camera camera) {
        Camera.Parameters parameters = camera.getParameters();
        int width = parameters.getPreviewSize().width;
        int height = parameters.getPreviewSize().height;
        YuvImage yuv = new YuvImage(data, parameters.getPreviewFormat(), width, height, null);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        yuv.compressToJpeg(new Rect(0, 0, width, height), 100, out);

        byte[] bytes = out.toByteArray();
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inMutable = true;
        return RotateBitmap(BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options), 90);
    }

    private roc_template getTemplateFromBitmap(Bitmap bitmap) {
        roc_image ri = fromBitmap(bitmap);
        roc_template template = new roc_template();

        SWIGTYPE_p_size_t adaptive_minimum_size = roc.new_size_t();
        roc.roc_ensure(roc.roc_adaptive_minimum_size(ri, 0.08f, 20, adaptive_minimum_size));
        roc.roc_ensure(roc.roc_represent(ri, (long) (roc_algorithm_options.ROC_FRONTAL | roc_algorithm_options.ROC_DEMOGRAPHICS | roc_algorithm_options.ROC_FR_FAST), roc.size_t_value(adaptive_minimum_size), 1, 0.02f, -5.f, template));
        roc.roc_free_image(ri); // release image
        roc.delete_size_t(adaptive_minimum_size);// release minimum size
        if ((template.getAlgorithm_id() & (long) roc_algorithm_options.ROC_INVALID) != 0) {
            roc.roc_free_template(template);    // release template
            return null;
        } else {
            return template;
        }
    }

    private long getLastFaceInterval() {
        return System.currentTimeMillis() - lastFaceTimestamp;
    }

    private float getDistanceBetween2Point(float x1, float y1, float x2, float y2) {
        return (float) Math.sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
    }

    @Override
    public void onPreviewFrame(final byte[] data, final Camera camera) {
        if (!detectionRunning) {
            new Thread(new Runnable() {
                public void run() {
                    detectionRunning = true;

                    final Bitmap bitmap = getBitmapFromFrame(data, camera);
                    final roc_template template = getTemplateFromBitmap(bitmap);

                    if (template == null) {
                        //no face
                        if (getLastFaceInterval() > 1500) {
                            //hidden face mark animation
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    face_mark_iv.setVisibility(View.INVISIBLE);
                                    faceMarkAnimation.stop();
                                }
                            });
                        }
                        if (getLastFaceInterval() > 3000) {
                            //hidden temp result
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    updateTitle("Welcome");
                                    temp_layout.setVisibility(View.INVISIBLE);
                                }
                            });
                        }
                    } else {
                        String name = "";


                        for (Template t : templateList) {
                            SWIGTYPE_p_float f = roc.new_roc_similarity();
                            roc.roc_compare_templates(t.getTemplate(), template, f);
                            Log.v("aa", "=====" + roc.roc_similarity_value(f));
                            if (roc.roc_similarity_value(f) >= 0.95) {
                                name = " " + t.getName();
                            }
                        }


                        lastFaceTimestamp = System.currentTimeMillis();
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                face_mark_iv.setVisibility(View.VISIBLE);
                                faceMarkAnimation.start();
                            }
                        });
//                        final int left = (int) template.getDetection().getX() - ((int) template.getDetection().getWidth() / 2);
//                        final int top = (int) template.getDetection().getY() - ((int) template.getDetection().getHeight() / 2);
//                        final int right = (int) template.getDetection().getX() + ((int) template.getDetection().getWidth() / 2);
//                        final int bottom = (int) template.getDetection().getY() + ((int) template.getDetection().getHeight() / 2);

//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                Canvas canvas = new Canvas(bitmap);
//                                Paint p = new Paint();
//                                p.setStrokeWidth(10f);
//                                p.setColor(Color.RED);
//                                p.setStyle(Paint.Style.STROKE);
//                                canvas.drawRect(left, top, right, bottom, p);
//
//                                canvas.drawPoint(template.getDetection().getX(), template.getDetection().getY(), p);
//                                iv.setImageBitmap(bitmap);
//                            }
//                        });

//                        Log.v("TAG", "===" + template.getMd());
                        MetaData metaData = new Gson().fromJson(template.getMd(), MetaData.class);
//                        float eyeDistance = getDistanceBetween2Point(metaData.getLeftEyeX(), metaData.getLeftEyeY(), metaData.getRightEyeX(), metaData.getRightEyeX());
//                        Log.v("TAG", "eyeDistance====" + eyeDistance);


                        if (metaData.getMaskDetection().compareTo("NoMask") == 0) {
//                            updateText("No Mask !!!!", true);
                            updateTitle("Hi" + name + ", Please wear mask or get out");

                        } else {

//                            if (eyeDistance > 100) {

                            Log.v("TAG", "width======" + template.getDetection().getWidth());
                            double distance = (86 * 100) / template.getDetection().getWidth();
                            Log.v("TAG", "distance======" + distance);
                            if (distance < 100) {
//                            if (template.getDetection().getWidth() > 200) {
                                updateTitle("Detecting...");
                                getTemp();
                            } else {
//                                updateText("Hi, please come closer", false);
                                updateTitle("Hi" + name + ", Please come close");
                            }
                        }

                        roc.roc_free_template(template);
                    }

                    detectionRunning = false;
                }
            }).start();

        }
    }
}
