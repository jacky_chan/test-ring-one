package com.inreality.testrankone;

public class MetaData {
    private float African;
    private float Age;
    private float Anger;
    private String Artwork;
    private float Cartoon;
    private float ChinX;
    private float ChinY;
    private float Disgust;
    private float EastAsian;
    private String Emotion;
    private float European;
    private float Eye;
    private float FacialHair;
    private float Fear;
    private float Female;
    private String Gender;
    private String GeographicOrigin;
    private String Glasses;
    private float Human;
    private float IOD;
    private float Joy;
    private float LeftEyeX;
    private float LeftEyeY;
    private float Male;
    private float Mask;
    private String MaskDetection;
    private float Neutral;
    private float NoMask;
    private float None;
    private float NoseRootX;
    private float NoseRootY;
    private float OCD;
    private float Painting;
    private String Path;
    private float RightEyeX;
    private float RightEyeY;
    private float Sadness;
    private float SouthAsian;
    private float Sun;
    private float Surprise;


    // Getter Methods

    public float getAfrican() {
        return African;
    }

    public float getAge() {
        return Age;
    }

    public float getAnger() {
        return Anger;
    }

    public String getArtwork() {
        return Artwork;
    }

    public float getCartoon() {
        return Cartoon;
    }

    public float getChinX() {
        return ChinX;
    }

    public float getChinY() {
        return ChinY;
    }

    public float getDisgust() {
        return Disgust;
    }

    public float getEastAsian() {
        return EastAsian;
    }

    public String getEmotion() {
        return Emotion;
    }

    public float getEuropean() {
        return European;
    }

    public float getEye() {
        return Eye;
    }

    public float getFacialHair() {
        return FacialHair;
    }

    public float getFear() {
        return Fear;
    }

    public float getFemale() {
        return Female;
    }

    public String getGender() {
        return Gender;
    }

    public String getGeographicOrigin() {
        return GeographicOrigin;
    }

    public String getGlasses() {
        return Glasses;
    }

    public float getHuman() {
        return Human;
    }

    public float getIOD() {
        return IOD;
    }

    public float getJoy() {
        return Joy;
    }

    public float getLeftEyeX() {
        return LeftEyeX;
    }

    public float getLeftEyeY() {
        return LeftEyeY;
    }

    public float getMale() {
        return Male;
    }

    public float getMask() {
        return Mask;
    }

    public String getMaskDetection() {
        return MaskDetection;
    }

    public float getNeutral() {
        return Neutral;
    }

    public float getNoMask() {
        return NoMask;
    }

    public float getNone() {
        return None;
    }

    public float getNoseRootX() {
        return NoseRootX;
    }

    public float getNoseRootY() {
        return NoseRootY;
    }

    public float getOCD() {
        return OCD;
    }

    public float getPainting() {
        return Painting;
    }

    public String getPath() {
        return Path;
    }

    public float getRightEyeX() {
        return RightEyeX;
    }

    public float getRightEyeY() {
        return RightEyeY;
    }

    public float getSadness() {
        return Sadness;
    }

    public float getSouthAsian() {
        return SouthAsian;
    }

    public float getSun() {
        return Sun;
    }

    public float getSurprise() {
        return Surprise;
    }

    // Setter Methods

    public void setAfrican(float African) {
        this.African = African;
    }

    public void setAge(float Age) {
        this.Age = Age;
    }

    public void setAnger(float Anger) {
        this.Anger = Anger;
    }

    public void setArtwork(String Artwork) {
        this.Artwork = Artwork;
    }

    public void setCartoon(float Cartoon) {
        this.Cartoon = Cartoon;
    }

    public void setChinX(float ChinX) {
        this.ChinX = ChinX;
    }

    public void setChinY(float ChinY) {
        this.ChinY = ChinY;
    }

    public void setDisgust(float Disgust) {
        this.Disgust = Disgust;
    }

    public void setEastAsian(float EastAsian) {
        this.EastAsian = EastAsian;
    }

    public void setEmotion(String Emotion) {
        this.Emotion = Emotion;
    }

    public void setEuropean(float European) {
        this.European = European;
    }

    public void setEye(float Eye) {
        this.Eye = Eye;
    }

    public void setFacialHair(float FacialHair) {
        this.FacialHair = FacialHair;
    }

    public void setFear(float Fear) {
        this.Fear = Fear;
    }

    public void setFemale(float Female) {
        this.Female = Female;
    }

    public void setGender(String Gender) {
        this.Gender = Gender;
    }

    public void setGeographicOrigin(String GeographicOrigin) {
        this.GeographicOrigin = GeographicOrigin;
    }

    public void setGlasses(String Glasses) {
        this.Glasses = Glasses;
    }

    public void setHuman(float Human) {
        this.Human = Human;
    }

    public void setIOD(float IOD) {
        this.IOD = IOD;
    }

    public void setJoy(float Joy) {
        this.Joy = Joy;
    }

    public void setLeftEyeX(float LeftEyeX) {
        this.LeftEyeX = LeftEyeX;
    }

    public void setLeftEyeY(float LeftEyeY) {
        this.LeftEyeY = LeftEyeY;
    }

    public void setMale(float Male) {
        this.Male = Male;
    }

    public void setMask(float Mask) {
        this.Mask = Mask;
    }

    public void setMaskDetection(String MaskDetection) {
        this.MaskDetection = MaskDetection;
    }

    public void setNeutral(float Neutral) {
        this.Neutral = Neutral;
    }

    public void setNoMask(float NoMask) {
        this.NoMask = NoMask;
    }

    public void setNone(float None) {
        this.None = None;
    }

    public void setNoseRootX(float NoseRootX) {
        this.NoseRootX = NoseRootX;
    }

    public void setNoseRootY(float NoseRootY) {
        this.NoseRootY = NoseRootY;
    }

    public void setOCD(float OCD) {
        this.OCD = OCD;
    }

    public void setPainting(float Painting) {
        this.Painting = Painting;
    }

    public void setPath(String Path) {
        this.Path = Path;
    }

    public void setRightEyeX(float RightEyeX) {
        this.RightEyeX = RightEyeX;
    }

    public void setRightEyeY(float RightEyeY) {
        this.RightEyeY = RightEyeY;
    }

    public void setSadness(float Sadness) {
        this.Sadness = Sadness;
    }

    public void setSouthAsian(float SouthAsian) {
        this.SouthAsian = SouthAsian;
    }

    public void setSun(float Sun) {
        this.Sun = Sun;
    }

    public void setSurprise(float Surprise) {
        this.Surprise = Surprise;
    }
}