package com.inreality.testrankone;

import io.rankone.rocsdk.roc_template;

public class Template {
    private roc_template template;
    private String name;

    public Template(String name, roc_template template) {
        this.name = name;
        this.template = template;
    }

    public String getName() {
        return name;
    }

    public roc_template getTemplate() {
        return template;
    }

}
